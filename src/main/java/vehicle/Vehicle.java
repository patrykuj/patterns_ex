package vehicle;

import vehicle.enums.Direction;

/**
 * Created by epakarb on 2017-01-25.
 */
public interface Vehicle {
    void start();
    void stop();
    void turn(Direction direction);
    void breaking();
}
