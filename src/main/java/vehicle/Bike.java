package vehicle;

import vehicle.enums.Direction;

/**
 * Created by epakarb on 2017-01-25.
 */
public class Bike implements Vehicle{
    public void start() {
        System.out.println("Bike starts");
    }

    public void stop() {
        System.out.println("Bike stops");
    }

    public void turn(Direction direction) {
        System.out.println("Bike turns " + direction.getDirection());
    }

    public void breaking() {
        System.out.println("Bike is breaking");
    }
}
