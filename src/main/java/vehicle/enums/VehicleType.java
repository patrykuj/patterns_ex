package vehicle.enums;

/**
 * Created by epakarb on 2017-01-25.
 */
public enum VehicleType {
    CAR("CAR"), BIKE("BIKE"), SHOPPINGCART("SHOPPINGCART");

    private String type;

    VehicleType(String type) {

        this.type = type;
    }

    public String getType() {
        return type;
    }
}
