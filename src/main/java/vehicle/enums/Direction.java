package vehicle.enums;

/**
 * Created by epakarb on 2017-01-25.
 */
public enum Direction {
    LEFT("LEFT"), RIGHT("RIGHT");

    private String direction;

    Direction(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }
}
