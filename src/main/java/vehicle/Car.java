package vehicle;

import vehicle.enums.Direction;

/**
 * Created by epakarb on 2017-01-25.
 */
public class Car implements Vehicle{
    public void start() {
        System.out.println("Car's engine is starting");
    }

    public void stop() {
        System.out.println("Car's engine is stopping");
    }

    public void turn(Direction direction) {
        System.out.println("Car is turning " + direction.getDirection());
    }

    public void breaking() {
        System.out.println("Car is breaking");
    }
}
