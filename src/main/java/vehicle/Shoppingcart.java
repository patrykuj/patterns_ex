package vehicle;

import vehicle.enums.Direction;

/**
 * Created by epakarb on 2017-01-25.
 */
public class Shoppingcart implements Vehicle{
    public void start() {
        System.out.println("Shoppingcart has been pushed");
    }

    public void stop() {
        System.out.println("Shoppingcart stops");
    }

    public void turn(Direction direction) {
        System.out.println("Shoppingcart turns " + direction.getDirection());
    }

    public void breaking() {
        System.out.println("Shoppingcart can not breaks");
    }
}
